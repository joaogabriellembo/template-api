const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  api.post("/users", async (req, res) => {

    // check contract    
    const fields = ["name", "email", "password", "passwordConfirmation"];
    for (var i = 0; i < 4; i++) {
      if (!req.body.hasOwnProperty(fields[i])) {
        res.status(400).json({"error": "Request body had missing field " + fields[i] });
        return;
      }
    }

    //check formatting
    if (typeof req.body.name != "string" || req.body.name.length == 0) {
      res.status(400).json({"error": "Request body had malformed field name"});
      return;
    }

    if (typeof req.body.email != "string" || req.body.email.match(".*@\..*") == null) {
      res.status(400).json({"error": "Request body had malformed field email"});
      return
    }

    if (typeof req.body.password != "string" || req.body.password.match("^[A-z0-9]+$") == null 
        || req.body.password.length < 8 || req.body.password.length > 32) {
      res.status(400).json({"error": "Request body had malformed field password"});
      return;
    }

    if (!(req.body.password === req.body.passwordConfirmation)) {
      res.status(422).json({"error": "Password confirmation did not match"});
      return;
    }

    const database = mongoClient.db("api_service");
    const users = database.collection('users');
    const results = await users.findOne({"email": req.body.email});
    if (results != null) {
      res.status(409).json({"error": "This email is already registered"});
      return;
    }
    let id = uuid();

    let userAggregate = {"name": req.body.name, "email": req.body.email, "password": req.body.password};

    res.status(201).json({"user" :{"id": id, "name": req.body.name, "email": req.body.email}});

    const event = {"eventType": "UserCreated", "entityId": id, "entityAggregate": userAggregate};

    stanConn.publish('users', JSON.stringify(event), (err, guid) => {
      if (err) {
        console.log("Failed to publish UserCreated event");
      } else {
        console.log("Published UserCreated event with GUID: " + guid);
      }
    });
  });

  api.delete("/users/:uuid", async (req, res) => {
    

    if (!req.headers.hasOwnProperty("authentication")) {
      res.status(401).json({"error": "Access Token not found" });
      return;
    }

    const token = req.headers.authentication.split(" ")[1];

    const payload = jwt.decode(token);

    const tokenUuid = payload.id;

    if (!(req.params.uuid ===  tokenUuid)) {
      res.status(403).json({"error": "Access Token did not match User ID"});
      return;
    }

    const database = mongoClient.db("api_service");
    const users = database.collection('users');
    const results = await users.findOne({"id": req.params.uuid});
    if (results == null) {
      res.status(409).json({"error": "There is no user with this id"});
      return;
    }

    const event = {"eventType": "UserDeleted", "entityId": tokenUuid, "entityAggregate": {}};

    stanConn.publish('users', JSON.stringify(event), (err, guid) => {
      if (err) {
        console.log("Failed to publish UserDeleted event");
      } else {
        console.log("Published UserDeleted event with GUID: " + guid);
      }
    });

    res.status(200).json({"id": tokenUuid});
  })

  return api;
};
