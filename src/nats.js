const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */
    /* YOUR CODE GOES HERE */
    /* ******************* */

    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const subscription = conn.subscribe('users', opts);
    subscription.on('message', (msg) => {

      const eventString = msg.getData();
      console.log(eventString);
      const event = JSON.parse(eventString);
      if (event.eventType === "UserCreated") {

        const user = {"id": event.entityId, ...event.entityAggregate};
        const database = mongoClient.db("api_service");
        const users = database.collection('users');
        users.insertOne(user);
      }

      else if (event.eventType === "UserDeleted") {

        const id = event.entityId;

        const user = {"id": event.entityId, ...event.entityAggregate};
        const database = mongoClient.db("api_service");
        const users = database.collection('users');
        users.deleteOne({"id": id});

      }
    });
  });

  return conn;
};
